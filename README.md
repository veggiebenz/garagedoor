# NodeMCU Garage Door Opener - NodeMCU / ESP8266 Control Garage door with Alexa, no Alexa Skill required #

This is a project demonstrating ESP8266 control of Garage Door, including mobile friendly web site and Amazon Echo (Alexa) control.

The Alexa control is facilitated through Espalexa project, and does not require a cloud component; it all happens locally on your WiFi network.

Features:

* [ESPAsyncWebServer] (https://github.com/me-no-dev/ESPAsyncWebServer)
* [Espalexa] (https://github.com/Aircoookie/Espalexa)
* [ESPAsyncWiFiManager] (https://github.com/alanswx/ESPAsyncWiFiManager) - automatic access point if it does not find a known WiFi
* Mobile friendly web site

Caveats:

* Make sure to use a relay that is 3V DC rated.  I was not able to get this working with 5V relay.
* Place the sensor on the ceiling of the garage, looking down at the area the door will occupy when raised.  The ultrasonic sensor is set to detect "door up" when it is obstructed < 100 cm.  Otherwise it determines (since it can see to the floor) that garage door is "closed"
* You can use different GPIO pins than I have used; however you might have problems.  GPIO2 and GPIO16 for example, were floating voltage upon boot, which was enough to trigger the relay.  I didn't want my garage door to open any time there was a power fluctuation.
* Make sure to get a very good power supply.  Insufficient amperage can cause NodeMCU to crash when relay is triggered.

![esp_garage_door_bb.png](https://bitbucket.org/veggiebenz/garagedoor/raw/34d0586d1b9aaa79c8b0f9d243956c7b6976443e/docs/esp_garage_door_bb.png)

![screenshot](https://bitbucket.org/veggiebenz/garagedoor/raw/5b88d713d90404c9654b7e99d3894d44cff888d3/docs/garagedoor-screenshot.PNG)
