#define ESPALEXA_ASYNC //it is important to define this before #include <Espalexa.h>!
#include <Espalexa.h>

#ifdef ARDUINO_ARCH_ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
  #include <ESPmDNS.h>
  #include <SPIFFS.h>
#else
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
  #include <ESP8266mDNS.h>
  #include <FS.h>
#endif
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h> 

// NodeMCU Pin D1 > TRIGGER | Pin D2 > ECHO
#define TRIGGER 16       
#define ECHO    5
#define RELAY   4

const float default_duration = 1.5;
const char* host = "garagedoor";

Espalexa espalexa;

AsyncWebServer server(80);
DNSServer dns;
void garageDoorAction(EspalexaDevice* dev);

void setup () 
{
    Serial.begin ( 115200 );
    Serial.println ( "Initializing" );
    pinMode(TRIGGER, OUTPUT);
    pinMode(ECHO, INPUT);
    pinMode(BUILTIN_LED, OUTPUT);

	digitalWrite(RELAY, LOW); // turn relay off
    pinMode(RELAY, OUTPUT); // initialize pin as OUTPUT
	digitalWrite(RELAY, LOW); // turn relay off

    AsyncWiFiManager wifiManager(&server,&dns);
    wifiManager.autoConnect(host);

    if (!MDNS.begin(host)) {
        Serial.println("Error setting up MDNS responder!");
        while(1) {
            delay(1000);
        }
    }
    Serial.println("mDNS responder started");

    // Add service to MDNS-SD
    MDNS.addService("http", "tcp", 80);
 
    bool result = SPIFFS.begin();
    if (result != true) { SPIFFS.format();}
    Serial.println("SPIFFS opened: " + result);

    server.serveStatic("/", SPIFFS, "/").setDefaultFile("index.html");

    server.on("/distance", HTTP_GET, [](AsyncWebServerRequest *request){
        request->send ( 200, "text/plain", distanceGet() );
    });

    server.on("/operate", HTTP_ANY, [](AsyncWebServerRequest *request){
        float duration = default_duration;
        if (request->hasParam("duration")) {
            String temp = request->getParam("duration")->value();
            duration = atof(temp.c_str());
        }
        String msg = "Operating remote for " + String(duration) + " sec.";
        request->send ( 200, "text/plain", msg );
        triggerGarageDoorRemote(duration);
    });

    server.onNotFound([](AsyncWebServerRequest *request){
        if (!espalexa.handleAlexaApiCall(request)) //if you don't know the URI, ask espalexa whether it is an Alexa control request
        {
            request->send(404, "text/plain", "Not found");          //whatever you want to do with 404s
        }
    });

    // Define your devices here.
    espalexa.addDevice("Garage Door Opener", garageDoorAction, EspalexaDeviceType::onoff);      // non dimmable device
    espalexa.begin(&server); 
}


String distanceGet()
{
    long duration;
    int distance;
    //double duration, distance;
    digitalWrite(TRIGGER, LOW);  
    delayMicroseconds(2); 

    digitalWrite(TRIGGER, HIGH);
    delayMicroseconds(10); 

    digitalWrite(TRIGGER, LOW);
    duration = pulseIn(ECHO, HIGH);

    distance= duration*0.034/2;             //  distance = (duration/2) / 29.1;

    Serial.print("Distance: ");
    Serial.println(distance);

    String jsonobj = "{ \"distance\" : ";
    jsonobj += distance;
    jsonobj += " } ";
    return jsonobj;
}

void triggerGarageDoorRemote(float duration)
{
    int delay_duration = duration * 1000;
    Serial.print("Triggering garage door remote for duration (ms): ");
    Serial.println(delay_duration);

    digitalWrite(RELAY, HIGH); // turn relay on
    delay(delay_duration);
	digitalWrite(RELAY, LOW); // turn relay off
}

void garageDoorAction(EspalexaDevice* dev)      
{
    if (dev == nullptr) return; //this is good practice, but not required

    Serial.print("Device changed to ");
    if (dev->getValue()){
        Serial.println("ON");
    }
    else {
        Serial.println("OFF");
    }
    triggerGarageDoorRemote(default_duration);
}

void loop () {
    espalexa.loop();
    MDNS.update();
    delay(1);
}
